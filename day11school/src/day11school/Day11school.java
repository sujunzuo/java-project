/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day11school;

class Box {

    Box(int value) {
        this.value = value;
        count++;
        serialNumber = count;
    }

    @Override
    public String toString() {
        return String.format("value is:%d; serialNumber is:%d", value, serialNumber);
    }
    int value;
    static int count;
    int serialNumber;
}

/**
 *
 * @author mobileapps
 */
public class Day11school {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //System.out.printf("I have  %d boxes.\n", Box.count);
        Box b1 = new Box(5);
        System.out.println(b1);
        //System.out.printf("I have  %d boxes.\n", Box.count);
        Box b2 = new Box(58);
        System.out.println(b2);
        //System.out.printf("I have  %d boxes.\n", Box.count);
        Box b3 = new Box(115);
        System.out.println(b3);
        //System.out.printf("I have  %d boxes.\n", Box.count);

    }

}
