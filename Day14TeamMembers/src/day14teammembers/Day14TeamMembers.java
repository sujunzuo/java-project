/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day14teammembers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class Day14TeamMembers {

    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        // TODO code application logic here
        //new a hashmap
        HashMap<String, ArrayList<String>> playersByTeams = new HashMap<>();
        try (Scanner fileInput = new Scanner(new File("teams.txt"))) {
            while (fileInput.hasNext()) {
                String line = fileInput.nextLine();
                String data[] = line.split(":");
                if (data.length != 2) {
                    throw new IllegalArgumentException("Invalid number of data fields");
                }
                String teamName = data[0];
                String teamMembers = data[1];
                String members[] = teamMembers.split(",");

                for (int i = 0; i < members.length; i++) {
                    //myList.add(members[i]);
                    //hasmap
                    if (playersByTeams.containsKey(members[i])) {//alreay have the player's name
                        //get team name's ArrayList and add team'name
                        playersByTeams.get(members[i]).add(teamName);

                    } else { //new player's name
                        //creat a new key for hashmap
                        ArrayList<String> myList = new ArrayList<>();
                        myList.add(teamName);
                        playersByTeams.put((members[i]), myList);
                    }

                }
                //create an object of 
            }

        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println("Error in Reading file.");
            return;
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
            return;
        }
        // display result
        for (Map.Entry<String, ArrayList<String>> entry : playersByTeams.entrySet()) {
            String key = entry.getKey();
            System.out.print(key+" plays in:");
            ArrayList<String> value = entry.getValue();
            //teams'name
            boolean isFirst= true;
            for(String team: value){
                if(isFirst){
                  System.out.print(team);
                  isFirst=false;
                }else{
                                            
                   
                    System.out.print(","+team);
                }
                
            }
            System.out.println("");
        }

    }

}

