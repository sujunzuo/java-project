/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day14generic;

class Box<T> {
    void setValue(T value){
     this.value=value;
    }
    T getValue(){
        return value;
    }
    private T value;
}

public class Day14Generic {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Box<String> strBox= new Box<>();
        strBox.setValue("112");
     //   Box<int> intBox= new Box<>();
    }
    
}
