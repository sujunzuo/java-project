/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day14numberfrequence;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;

public class Day14NumberFrequence {

    static ArrayList<Integer> intArray = new ArrayList<>();
    static ArrayList<Integer> uniqueInts = new ArrayList<>();
    static ArrayList<IntFrequency> uniqueFre = new ArrayList<>();
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        // TODO code application logic here
        try (Scanner fileInput = new Scanner(new File("input.txt"))) {
            while (fileInput.hasNext()) {
                String token = fileInput.next();
                int num = Integer.parseInt(token);
                intArray.add(num);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println("Error in Reading file.");
            return;
        }
        //continue 
        for (int n : intArray) {
            IntFrequency nfreq = new IntFrequency(n);
            if (!uniqueFre.contains(nfreq)) {
                uniqueFre.add(nfreq);
            }
        }
        System.out.println("Unique interges:");
        for (IntFrequency intfreq : uniqueFre) {
            System.out.print(intfreq + ",");
        }
        // counting occurentcs
        for (IntFrequency intfreq : uniqueFre) {
            
            for (int n : intArray) {
                if (n == intfreq.number) {
                    intfreq.count++;
                }
            }
            System.out.println("Item:" + intfreq.number + " has occurnted " + intfreq.count + " times.");
        }

    }

}

class IntFrequency implements Comparable<IntFrequency> {

    int number;
    int count;

    public IntFrequency(int number) {
        this.number = number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getNumber() {
        return number;
    }

    public int getCount() {
        return count;
    }

    @Override
    public int compareTo(IntFrequency o) {
        return count - o.count;
    }

    @Override
    public String toString() {
        return String.format("%d[%d]", number, count); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean equals(Object obj) {
        IntFrequency other = (IntFrequency) obj;
        return number == other.number;
    }

}
