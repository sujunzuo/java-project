/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day02geo;

/**
 *
 * @author mobileapps
 */
public class Day02Geo {

    public static void main(String[] args) {
        Point point = new Point("yellow");
        point.print();
        Rectangle rectangle = new Rectangle("green", 1.45, 4.87);
        rectangle.print();
    }
    
}
abstract class GeoObj {
    GeoObj(String color) { 
        setColor(color);
    }
    private String color;
    abstract double getSurface() ;
            
    abstract double getCircumPerim() ;
    abstract int getVerticesCount() ;
    abstract int getEdgesCount(); 
    abstract void print() ; 

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        if (color.length() < 1) {
            throw new IllegalArgumentException("Color must be at least 1 character long");
        }
        this.color = color;
    }
}

class Point extends GeoObj {
    Point(String color) {
        super(color);
    }
    @Override
    double getSurface() { return 0; }
    @Override
    double getCircumPerim() { return 0; }
    @Override
    int getVerticesCount() { return 1; }
    @Override
    int getEdgesCount() { return 0; }
    @Override
    void print() { System.out.printf("Point: color=%s\n", getColor()); }  
    
}

class Rectangle extends GeoObj {
    Rectangle(String color, double width, double height) {
        super(color);
        setWidth(width);
        setHeight(height);
    }
    @Override
    double getSurface() { return width*height; }
    @Override
    double getCircumPerim() { return width*2 + height*2; }
    @Override
    int getVerticesCount() { return 4; }
    @Override
    int getEdgesCount() { return 4; }
    @Override
    void print() { System.out.printf("Rectangle: color=%s, width=%.2f, height=%.2f\n",
            getColor(), getWidth(), getHeight()); }  
    
    private double width;
    private double height;

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        if (width < 0) {
            throw new IllegalArgumentException("Width must not be negative");
        }
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        if (height < 0) {
            throw new IllegalArgumentException("Height must not be negative");
        }
        this.height = height;
    }
    
}

class Square extends Rectangle {
    Square(String color, double edgeSize) {
        super(color, edgeSize, edgeSize);
    }
 
    @Override
    void print() { System.out.printf("Square: color=%s, width=%.2f, height=%.2f\n",
            getColor(), getWidth(), getHeight()); }    
}




