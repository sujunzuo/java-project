/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz3household;

import java.text.ParseException;

class DataInvalidException extends RuntimeException {

    public DataInvalidException(String message) {
        super(message);
    }
}

interface Marshallable {

    String toDataString();
}

public abstract class HomeItem implements Marshallable {

    private String description; // 2-50 characters except semicolon ;
    private double price; // 0-999999 floating point
    private String category;
    
    //leave implement method to subclass
    public abstract String toDataString();

    //*************factory method*********
    static HomeItem createFromDataLine(String line) throws ParseException {
        String[] data = line.split(";");
        double price=0;
        boolean isFurniture=false;
        boolean isElectronics=false;
        String description="";
        HomeItem homeitem=null;
        
        if (data.length != 4) {
            throw new DataInvalidException("Invalid number of data fields");
        }
        if(data[0].equals("Furniture")){ //Furniture
            isFurniture=true;
            
        }else if(data[0].equals("Electronics")){ //Electronics
            isElectronics=true;
        }else { //error in data[0]
            throw new DataInvalidException("Type of HomeItem should be Furniture or Electronics");
        }
        price = Double.parseDouble(data[1]);// possible:  PareseException
        description =data[2];
        
        if(isFurniture) {
            homeitem =(HomeItem) new FurnitureItem("Furniture",description,price,FurnitureItem.HomeItemType.valueOf(data[3]));
        }
        if(isElectronics) {
            homeitem =(HomeItem) new ElectronicsItem("Electronics",description,price,data[3]);
        }
                 
        return homeitem;
        
    } // factory method
    //constractors
    public HomeItem() { }
    public HomeItem(String category,String description, double price) {
        setCategory(category);
        setDescription(description);        
        setPrice(price);
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setDescription(String description) {
        
        description = description.trim();
        if ((description.length() < 2) || (description.length() > 50)) {
            throw new DataInvalidException("Description should be 2-50 characters.");
        }
        this.description = description;
    }

    public void setPrice(double price) {
        if ((price < 0) || (price > 999999)) {
            throw new DataInvalidException("Price should be greater than 0 and less than 999999.");
        }
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public double getPrice() {
        return price;
    }

    public String getCategory() {
        return category;
    }
   
    
}

class FurnitureItem extends HomeItem {

    enum HomeItemType {
        Desk, Chair, Table, Sofa, Bed
    }
    private HomeItemType itemType;

    @Override
    public String toDataString() {
        return String.format("%s;%.2f;%s;%s", "Furniture", this.getPrice(),this.getDescription(), itemType);
    }

    public HomeItemType getItemType() {
        return itemType;
    }

    public void setItemType(HomeItemType itemType) {
        this.itemType = itemType;
    }
    //constructors
    public FurnitureItem() {
        
    }
    public FurnitureItem(String category, String description, double price, HomeItemType itemType) {
        super(category,description,price);
        
        this.itemType = itemType;
    }

    @Override
    public String toString() {
        
        return String.format("%s: %s  price is %.2f; item type is:%s", "Furniture", this.getDescription(),this.getPrice(), itemType);
    }
    
    
}

class ElectronicsItem extends HomeItem {

    private String model; // 2-50 characters except semicolon ;

    
    //constructors
    public ElectronicsItem() {
        
    }

    public ElectronicsItem(String category, String description, double price,String model) {
        super(category,description,price);
        setModel(model);
    }

    public void setModel(String model) {
        if ((model.length() < 2) || (model.length() > 50)) {
            throw new DataInvalidException("Model should be 2-50 characters.");
        }
        if (model.contains(";")){
            throw new DataInvalidException("Model should not include a character of semicolon(;)");
        }    
        this.model = model;
    }

    public String getModel() {
        return model;
    }
    
    @Override
    public String toDataString() {
        return String.format("%s;%.2f;%s;%s", "Electronics", this.getPrice(),this.getDescription(), model);
    }

    @Override
    public String toString() {
        return String.format("%s: %s  price is %.2f; model is:%s", "Electronics", this.getDescription(),this.getPrice(), model);
    }
    
}
