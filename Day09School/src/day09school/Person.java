/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day09school;

/**
 *
 * @author mobileapps
 */
public class Person {
    String name;
    int age;
    Person(){}

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
    
    @Override
    public String toString() {
        return String.format("%s is %d y/o", name, age);
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
    
}
