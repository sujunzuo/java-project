/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day09school;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author mobileapps
 */
public class Day09People extends javax.swing.JFrame {

    /**
     * Creates new form Day09People
     */
    DefaultListModel<Person> modelPeopleList = new DefaultListModel<>();
    final String DATA_FILE_NAME="todos.data";
            
    public Day09People() {
        initComponents();
        FileFilter fileFilter = new FileNameExtensionFilter("Text files (*.txt)", "txt");
        fileChooser.setFileFilter(fileFilter);
        

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fileChooser = new javax.swing.JFileChooser();
        bggroupSortBy = new javax.swing.ButtonGroup();
        tfName = new javax.swing.JTextField();
        btAddPerson = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        tfAge = new javax.swing.JTextField();
        btDelete = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstPeople = new javax.swing.JList<>();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        rbSortByName = new javax.swing.JRadioButtonMenuItem();
        rbSortByAge = new javax.swing.JRadioButtonMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tfName.setName(""); // NOI18N

        btAddPerson.setText("add");
        btAddPerson.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAddPersonActionPerformed(evt);
            }
        });

        jLabel1.setText("Name:");

        jLabel2.setText("age:");

        tfAge.setName(""); // NOI18N

        btDelete.setText("delete");
        btDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btDeleteActionPerformed(evt);
            }
        });

        lstPeople.setModel(modelPeopleList);
        lstPeople.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(lstPeople);

        jMenu1.setText("File");

        jMenuItem1.setText("Open...");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Save as...");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem3.setText("Exit");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clickExit(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Sort by");

        bggroupSortBy.add(rbSortByName);
        rbSortByName.setSelected(true);
        rbSortByName.setText("Name");
        rbSortByName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbSortByNameActionPerformed(evt);
            }
        });
        jMenu2.add(rbSortByName);

        bggroupSortBy.add(rbSortByAge);
        rbSortByAge.setText("Age");
        rbSortByAge.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbSortByAgeActionPerformed(evt);
            }
        });
        jMenu2.add(rbSortByAge);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(54, 54, 54)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel1)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel2)
                            .addGap(18, 18, 18)
                            .addComponent(tfAge, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btDelete)
                        .addGap(18, 18, 18)
                        .addComponent(btAddPerson)))
                .addContainerGap(77, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(89, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(tfAge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(61, 61, 61)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btAddPerson)
                            .addComponent(btDelete))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // open file
        if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            try (Scanner fileInput = new Scanner(file)) {
                modelPeopleList.clear();
                while (fileInput.hasNextLine()) {
                    try {
                        String line = fileInput.nextLine();
                        String[] data = line.split(";");
                        if (data.length != 2) {
                            JOptionPane.showMessageDialog(this,
                                    "Invalid data in line:" + line,
                                    "Data validation error",
                                    JOptionPane.ERROR_MESSAGE);
                            continue;
                        }
                        String name = data[0];
                        int age = Integer.parseInt(data[1]);
                        Person person = new Person(name, age);
                        modelPeopleList.addElement(person);

                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(this,
                                "Invalid data,expected a interger",
                                "Data validation error",
                                JOptionPane.ERROR_MESSAGE);

                    }
                }

            } catch (IOException ex) {
                JOptionPane.showMessageDialog(this,
                        "Error reading to file.\n" + ex.getMessage(),
                        "File access error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }

    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void clickExit(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clickExit
        // TODO add your handling code here:
        dispose();
        //exit
        System.exit(0);
    }//GEN-LAST:event_clickExit

    private void btAddPersonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAddPersonActionPerformed
        // TODO add your handling code here:
        try {
            String name = tfName.getText();
            if (name.isEmpty()) {
                JOptionPane.showMessageDialog(this,
                        "Name must not be empty.",
                        "Input warning",
                        JOptionPane.WARNING_MESSAGE);
                return;
            }
            String ageStr = tfAge.getText();
            int age = Integer.parseInt(ageStr);
            if (age < 0 || age > 150) {
                JOptionPane.showMessageDialog(this,
                        "Age must be within 0-150 range.",
                        "Input warning",
                        JOptionPane.WARNING_MESSAGE);
                return;
            }
            Person p = new Person(name, age);
            modelPeopleList.addElement(p);
            tfName.setText("");
            tfAge.setText("");
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(this,
                    "Age must be an integer value.",
                    "Input warning",
                    JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btAddPersonActionPerformed

    private void btDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btDeleteActionPerformed
        // TODO add your handling code here:
        //ask the user for confiration -Yes/No or OK/Cancel dialog
        Person p = lstPeople.getSelectedValue();
        if (p != null) {
            int retVal = JOptionPane.showConfirmDialog(this, "Are you want to delete" + p.toString(), "Confirm to delete", JOptionPane.YES_NO_OPTION);
            if (retVal == JOptionPane.YES_OPTION) {

                //delete
                modelPeopleList.removeElement(p);
            } else {
                //do nothing
            }
        } else {
            JOptionPane.showMessageDialog(this, "No selected item.", " error", JOptionPane.ERROR_MESSAGE);
        }
        /*for(Person p: lstPeople.getSelectedValuesList()){
            modelPeopleList.removeElement(p);
        } */
    }//GEN-LAST:event_btDeleteActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            try (PrintWriter pw = new PrintWriter(file)) {
                for (int i = 0; i < modelPeopleList.size(); i++) {
                    Person p = modelPeopleList.get(i);
                    pw.printf("%s;%d\n", p.name, p.age);
                }
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(this, "Error writing to file.\n" + ex.getMessage(), "File access error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void rbSortByNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbSortByNameActionPerformed
        // TODO add your handling code here:
        //sort by name
        ArrayList<Person> sortList = new ArrayList<>();
        sortList = Collections.list(modelPeopleList.elements());
        
        sortList.sort(Comparator.comparing(Person::getName));
        modelPeopleList.clear();
        for(Person p: sortList){
            modelPeopleList.addElement(p);
        }
        //modelPeopleList = Collections.list(sortList.elements());
        
    }//GEN-LAST:event_rbSortByNameActionPerformed

    private void rbSortByAgeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbSortByAgeActionPerformed
        // TODO add your handling code here:
        //sort by name
        ArrayList<Person> sortList = new ArrayList<>();
        sortList = Collections.list(modelPeopleList.elements());
        
        sortList.sort(Comparator.comparingInt(Person::getAge));
        modelPeopleList.clear();
        for(Person p: sortList){
            modelPeopleList.addElement(p);
        }
    }//GEN-LAST:event_rbSortByAgeActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Day09People.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Day09People.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Day09People.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Day09People.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Day09People().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bggroupSortBy;
    private javax.swing.JButton btAddPerson;
    private javax.swing.JButton btDelete;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JList<Person> lstPeople;
    private javax.swing.JRadioButtonMenuItem rbSortByAge;
    private javax.swing.JRadioButtonMenuItem rbSortByName;
    private javax.swing.JTextField tfAge;
    private javax.swing.JTextField tfName;
    // End of variables declaration//GEN-END:variables
}
