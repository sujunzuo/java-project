/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day14autocalc;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;


/**
 *
 * @author mobileapps
 */
public class Day14AutoCalc {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        LoadDataFromFile();
        System.out.println("In my Token Stack:");
        System.out.println(myTokenList);
        Token t;
        //display
        
        do {
           t=myTokenList.pop();
           if (t!=null)
                 System.out.println(t);
           else
               break;
       } while (true);
    }
    static final String DATA_FILE_NAME="input.txt";
    static LIFOStatck<Token> myTokenList= new LIFOStatck<>();
    
            
    static void LoadDataFromFile() {
        File file = new File(DATA_FILE_NAME);
        try (Scanner fileInput = new Scanner(file)) {
            //modelTodoList.clear(); append to list ,not only from file

            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                try {
                    Token token = new Token(line);
                    myTokenList.push(token);

                } catch (InputInvalidException | ParseException ex) {
                    System.out.println("Error in Create new object: " + ex.getMessage());
                    
                }

            }

        } catch (IOException ex) {
            
        }
        
    }
    
}
class Token {
    enum Type {Number, Operation}
    Type type;
    double number;
    char operation;

    public Token(Type type, double number, char operation) {
        setType(type);
        setNumber(number);
        setOperation(operation);
    }
    public Token(String dataLine) throws ParseException{
        //wait to add later
        
        String[] data = dataLine.split(":");
        
        if (data.length != 2) {
            throw new InputInvalidException("Invalid number of data fields");
        }
        if(data[0].equals("push")){
            setType(Token.Type.valueOf("Number")); 
            setNumber(Double.parseDouble(data[1]));
            setOperation(' ');
        } else if(data[0].equals("operation")){
            setType(Token.Type.valueOf("Operation")); 
            setNumber(0);
            if (data[1].length()!=1)
                throw new InputInvalidException("Invalid input of operation.");
            setOperation(data[1].charAt(0));
        } else{
            throw new InputInvalidException("Invalid input of push or operation.");
        }
            
        

    }

    @Override
    public String toString() {
        return "Token{" + "type=" + type + ", number=" + number + ", operation=" + operation + '}';
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setNumber(double number) {
        this.number = number;
    }

    public void setOperation(char operation) {
        if((operation=='+')|(operation=='-')|(operation=='*')|(operation=='/')|(operation=='=')|(operation==' '))
           this.operation = operation;
        else
            throw new InputInvalidException("Invalid input of operation.");
    }
    
    
}

class LIFOStatck<T> {
	private ArrayList<T> storage = new ArrayList<>();
	void push(T t){
            storage.add(0,t);
            
            
        }
	T pop(){
            if (storage.isEmpty())
                return null;
            
            return storage.remove(0);
            
        }

        @Override
        public String toString(){
            String result="LIFO Stack:";
            for(T item: storage){
                result += item +",";
            }
            return result;
        }
}
class InputInvalidException extends RuntimeException {

    public InputInvalidException(String message) {
        super(message);
    }
}