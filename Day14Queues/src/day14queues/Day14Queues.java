/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day14queues;

import java.util.ArrayList;

/**
 *
 * @author mobileapps
 */
public class Day14Queues {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       LIFO<Integer> myStack= new LIFO<>();
       FIFO<Integer> myQueue= new FIFO<>();
       Integer myInt=null;
       
       myStack.push(1);
       myStack.push(100);
       myStack.push(200);
       myStack.push(100);
       myStack.push(99);
       System.out.println("begin pop stack");
       do {
           myInt=myStack.pop();
           if (myInt!=null)
                 System.out.println(myInt);
           else
               break;
       } while (true);
       myQueue.add(3);
       myQueue.add(300);
       myQueue.add(305);
       myQueue.add(300);
       myQueue.add(999);
       System.out.println(myQueue);
       System.out.println("begin remove queue");
       do {
           myInt=myQueue.remove();
           if (myInt!=null)
                 System.out.println(myQueue);
           else
               break;
       } while (true);
    }
    
    
}
class LIFO<T> {
	private ArrayList<T> storage = new ArrayList<>();
	void push(T t){
            storage.add(0,t);
            
            
        }
	T pop(){
            if (storage.isEmpty())
                return null;
            
            return storage.remove(0);
            
        }

        
}

class FIFO<T> {
	private ArrayList<T> storage = new ArrayList<>();
	void add(T t){
             storage.add(t);
        }
	T remove(){
            if (storage.isEmpty())
                return null;
            return storage.remove(0);
        }
        @Override
        public String toString(){
            String result="FIFO Queue:";
            for(T item: storage){
                result += item +",";
            }
            return result;
        }
}