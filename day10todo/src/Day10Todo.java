

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fangzhai
 */
public class Day10Todo extends javax.swing.JFrame {
DefaultListModel<Todo> modelTodoList = new DefaultListModel<>();
    /**
     * Creates new form Day10Todos
     */
    public Day10Todo() {
        initComponents();
        FileFilter fileFilter = new FileNameExtensionFilter("text files (*.txt)", "txt");
        fileChooser.setFileFilter(fileFilter);
        LoadDataFromFile(true);
        fileChooser.setVisible(false);
    }
    final String DATA_FILE_NAME = "todos.data";
    
    void LoadDataFromFile(boolean fromDefault) {
        //Parameter: fromDefault : true from default file name DATA_FILE_NAME; false from fileChooser dialog
        File file;
        SimpleDateFormat fileDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        
        if(fromDefault){
            file=new File(DATA_FILE_NAME);
            
        }else{
            fileChooser.setVisible(true);
            if(fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
               file = fileChooser.getSelectedFile();
            }else{
                return;
            }
        }
        try(Scanner fileInput =new Scanner(file)){
            modelTodoList.clear();
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                String[] data = line.split(":");
                //FIXME: make sure line has at least 3 items
                if (data.length != 3) {
                    //System.out.println("Error in line: invalid number of fields");
                    JOptionPane.showMessageDialog(this,
                                    "Invalid data in line:" + line,
                                    "invalid number of fields",
                                    JOptionPane.ERROR_MESSAGE);
                    continue;
                }
                try {
                    String name = data[0];
                    String str_date = data[1];
                    Date startDate = fileDateFormat.parse(str_date);
                    String str_isdone = data[2];
                    boolean isDone=false;
                    
                    if(Integer.parseInt(str_isdone)==0)
                        isDone=false;
                    else
                        isDone=true;
                    Todo todo =new Todo(name,startDate,isDone);
                    modelTodoList.addElement(todo);
                    
              

                } catch (IllegalArgumentException ex) {
                    //System.out.println("Error in Create new object: " + ex.getMessage());
                   ex.printStackTrace();//it is used to display where is the expection happened.
                   JOptionPane.showMessageDialog(this,
                                    "Invalid data in line:" + line,
                                    "Error in Create new object",
                                    JOptionPane.ERROR_MESSAGE);

                } catch (ParseException ex) {
                    //System.out.println("Error in parse string to Date.");
                    JOptionPane.showMessageDialog(this,
                                    "Invalid data in line:" + line,
                                    "Error in parse string to Date or parse string to integer",
                                    JOptionPane.ERROR_MESSAGE);
                }

            }

        }
        catch (IOException ex) {
                JOptionPane.showMessageDialog(this,
                        "Error reading to file.\n" + ex.getMessage(),
                        "File access error",
                        JOptionPane.ERROR_MESSAGE);
         }
    }
    
    void SaveDataToFile(boolean fromDefault) {
                //Parameter: fromDefault : true from default file name DATA_FILE_NAME; false from fileChooser dialog
        File file;
        
        if(fromDefault){
            file=new File(DATA_FILE_NAME);
            
        }else{
            fileChooser.setVisible(true);
            if(fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
               file = fileChooser.getSelectedFile();
            }else{
                return;
            }
        }
        try(PrintWriter pw =new PrintWriter(file)){
            for (int i = 0; i < modelTodoList.size(); i++) {
                    Todo t = modelTodoList.get(i);
                    pw.println(t.toDataString());
                }
            
        }
        catch (IOException ex) {
                JOptionPane.showMessageDialog(this,
                        "Error writing to file.\n" + ex.getMessage(),
                        "File access error",
                        JOptionPane.ERROR_MESSAGE);
         }
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstTodos = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cbIsDone = new javax.swing.JCheckBox();
        tfTask = new javax.swing.JTextField();
        tfDueDate = new javax.swing.JTextField();
        btAddTodo = new javax.swing.JButton();
        btDeleteTodo = new javax.swing.JButton();
        fileChooser = new javax.swing.JFileChooser();
        btUpdateTodo = new javax.swing.JButton();
        comboUrgency = new javax.swing.JComboBox<>();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        lstTodos.setModel(modelTodoList);
        lstTodos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstTodos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstTodosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(lstTodos);

        jLabel1.setText("Task:");

        jLabel2.setText("Due date(y/m/d):");

        cbIsDone.setText("is done?");

        btAddTodo.setLabel("Add todo");
        btAddTodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAddTodoActionPerformed(evt);
            }
        });

        btDeleteTodo.setLabel("Delete todo");
        btDeleteTodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btDeleteTodoActionPerformed(evt);
            }
        });

        btUpdateTodo.setText("Update todo");
        btUpdateTodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btUpdateTodoActionPerformed(evt);
            }
        });

        comboUrgency.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Later", "Normal", "Ugent" }));

        jMenu1.setText("File");

        jMenuItem1.setText("Import data from file and append to list ...");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Export data to file...");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem3.setText("Exit");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(fileChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 387, Short.MAX_VALUE)
                        .addGap(18, 18, 18)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(tfTask, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(tfDueDate, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addComponent(cbIsDone)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(btAddTodo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btUpdateTodo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btDeleteTodo, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(comboUrgency, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1)
                            .addComponent(tfTask, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(tfDueDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(23, 23, 23)
                        .addComponent(cbIsDone)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(comboUrgency, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btAddTodo)
                        .addGap(18, 18, 18)
                        .addComponent(btDeleteTodo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btUpdateTodo))
                    .addComponent(fileChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btAddTodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAddTodoActionPerformed
        // TODO add your handling code here:
        try {
            String task = tfTask.getText();
            String dueDateStr = tfDueDate.getText();
            Date dueDate = Todo.sdf.parse(dueDateStr);
            boolean isDone = cbIsDone.isSelected();
            Todo todo = new Todo(task, dueDate, isDone);
            modelTodoList.addElement(todo);
            tfTask.setText("");
            tfDueDate.setText("");
        } catch (IllegalArgumentException ex) {
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "Input error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (ParseException ex) {
            JOptionPane.showMessageDialog(this,
                    "Please enter valid date in y/m/d format",
                    "Input error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btAddTodoActionPerformed

    private void btDeleteTodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btDeleteTodoActionPerformed
        // TODO add your handling code here:
         Todo todo = lstTodos.getSelectedValue();
        if (todo == null) {
            JOptionPane.showMessageDialog(this,
                    "Please select an item first",
                    "Input error",
                    JOptionPane.WARNING_MESSAGE);
            return;
        }
        // ask the user for confirmation - OK/Cancel dialog
        Object[] options = {"Delete", "Cancel"};
        int result = JOptionPane.showOptionDialog(this,
                "Are you sure you want to delete\n" + todo,
                "Confirm action",
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[1]);
        if (result == JOptionPane.OK_OPTION) {
            modelTodoList.removeElement(todo);
        }
        //test String.format
        /*String task="mytask";
        String sdf="2019/01/31";
        boolean isDone=false;
        System.out.println(String.format("%s:%s:%d",task,sdf,isDone?1:0));*/
        
    }//GEN-LAST:event_btDeleteTodoActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
        dispose();
        SaveDataToFile(true);
        System.exit(0);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        //System.out.println("WindowClosingDemo.windowClosing");
        SaveDataToFile(true);
    }//GEN-LAST:event_formWindowClosing

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        LoadDataFromFile(false);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        SaveDataToFile(false);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void lstTodosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstTodosMouseClicked
        // TODO add your handling code here:
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
      // System.out.println("click in list");
        Todo t=lstTodos.getSelectedValue();
        if (t==null) return;
        
        tfTask.setText(t.getTask());
        tfDueDate.setText(sdf.format(t.getDueDate()));
        cbIsDone.setSelected(t.isIsDone());
        
            
    }//GEN-LAST:event_lstTodosMouseClicked

    private void btUpdateTodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btUpdateTodoActionPerformed
        // TODO add your handling code here:
        Todo t=lstTodos.getSelectedValue();
        if (t==null) return;
        try{
            t.setTask(tfTask.getText());
            t.setDueDate(Todo.sdf.parse(tfDueDate.getText()));
            t.setIsDone(cbIsDone.isSelected());
            lstTodos.repaint();
        }catch (ParseException | IllegalArgumentException ex){
            JOptionPane.showMessageDialog(this,
                    "Please enter correct format.\n"+ex.getMessage(),
                    "Input error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btUpdateTodoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Day10Todos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Day10Todos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Day10Todos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Day10Todos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Day10Todo().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAddTodo;
    private javax.swing.JButton btDeleteTodo;
    private javax.swing.JButton btUpdateTodo;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JCheckBox cbIsDone;
    private javax.swing.JComboBox<String> comboUrgency;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList<Todo> lstTodos;
    private javax.swing.JTextField tfDueDate;
    private javax.swing.JTextField tfTask;
    // End of variables declaration//GEN-END:variables
}
class Todo {
    enum Urgency {Later,Normal,Urgent}
    static SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    
    public Todo(String task, Date dueDate, boolean isDone) {
        setTask(task);
        setDueDate(dueDate);
        setIsDone(isDone);
        
    }    
    
    private String task;
    private Date dueDate;
    private boolean isDone;
    private Urgency urgency;//field added, use Combo-boxe  in GUI
    
    
    
    @Override
    public String toString() {
        return String.format("%s is due on %s and is %s",
                task, sdf.format(dueDate), isDone ? "done" : "not done");
    }
    public String toDataString() {
        return String.format("%s:%s:%d",task,sdf.format(dueDate),isDone?1:0);
       
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        if (task.length() < 1) {
            throw new IllegalArgumentException("Task must be at least 1 character long");
        }
        this.task = task;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dueDate);
        int year = calendar.get(Calendar.YEAR);
        if (year < 1900 | year > 2099) {
            throw new IllegalArgumentException("Year must be within 1900 to 2099 range");
        }
        this.dueDate = dueDate;
    }

    public boolean isIsDone() {
        return isDone;
    }

    public void setIsDone(boolean isDone) {
        this.isDone = isDone;
    }

    public void setUrgency(Urgency urgency) {
        this.urgency = urgency;
    }
    
    
    
}
