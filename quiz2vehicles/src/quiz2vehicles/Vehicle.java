package quiz2vehicles;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mobileapps
 */
public class Vehicle {

    enum Type {
        Car, Truck, Motobike
    }
    static SimpleDateFormat sdfInput = new SimpleDateFormat("yyyy/MM/dd");
    static SimpleDateFormat sdfFile = new SimpleDateFormat("yyyy-MM-dd");
    static SimpleDateFormat sdfDisplay = new SimpleDateFormat("MMM dd,yyyy");
    
    private Type type;
    private String description; // 2-50 characters
    private double weightKg;
    private String regPlate; // 2-10 uppercase characters and letters, nothing else
    private Date regExpires; // year 1900-2100

    public Vehicle(Type type, String description, double weightKg, String regPlate, Date regExpires) {
        setType(type);
        setDescription(description);
        setWeightKg(weightKg);
        setRegPlate(regPlate);
        setRegExpires(regExpires);

    }

    public Vehicle(String dataLine) throws ParseException{
        //wait to add later
        
        String[] data = dataLine.split("|");
        if (data.length != 5) {
            throw new InvalidDataProvidedException("Invalid number of data fields");
        }
        setType(Vehicle.Type.valueOf(data[0])); // possible: InvalidDataProvidedException
        setDescription(data[1]);
        setWeightKg(Double.parseDouble(data[2])); // possible: InvalidDataProvidedException or PareseException
        setRegPlate(data[3]);
        setRegExpires(Vehicle.sdfFile.parse(data[4]));
        

    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setDescription(String description) {
        description = description.trim();
        if ((description.length() < 2) || (description.length() > 50)) {
            throw new InvalidDataProvidedException("Description should be 2-50 characters.");
        }
        this.description = description;
    }

    public void setWeightKg(double weightKg) {
        if ((weightKg < 0) || (weightKg > 5000)) {
            throw new InvalidDataProvidedException("Weight of Kg should be greater than 0.");
        }
        this.weightKg = weightKg;
    }

    public void setRegPlate(String regPlate) {
        regPlate = regPlate.trim();
        if (!regPlate.matches("[a-zA-Z\\d]{2,10}")) {
            throw new InvalidDataProvidedException("The plate number should be 2-10 uppercase letters or digits.");
        }
        this.regPlate = regPlate;
    }

    public void setRegExpires(Date regExpires) {
        int theYear = regExpires.getYear();
        if (theYear > 200 || theYear < 0) {
            throw new InvalidDataProvidedException("The date of expires should between year of 1900 and 2100.");
        }
        this.regExpires = regExpires;
    }
    @Override
    public String toString() {
        return String.format("%s is %s, weighs %.0f reg plate  %s valid until %s",
                type, description,weightKg, regPlate,sdfDisplay.format(regExpires));
    }

    public String toDataString() {
        return String.format("%s|%s|%.0f|%s|%s", type, description, weightKg, regPlate,sdfFile.format(regExpires));
    }

    public Type getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public double getWeightKg() {
        return weightKg;
    }

    public String getRegPlate() {
        return regPlate;
    }

    public Date getRegExpires() {
        return regExpires;
    }

}
