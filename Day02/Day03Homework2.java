/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03homework;
import java.util.ArrayList;
import java.util.Random;
/**
 *
 * @author Su,Junzuo
 */
class RandomStore{
    ArrayList<Integer> intHistory =new ArrayList<>();
    Random obj_random =new Random();
    int nextInt(int minIncl, int maxExcl){
        
        int int_rand=obj_random.nextInt(maxExcl - minIncl)+ minIncl;
        intHistory.add(int_rand);
        return int_rand;
    }
    void printHistory(){
        boolean isFirst=true;
        for(int v: intHistory){
            if(isFirst){
              System.out.print(v);
              isFirst=false;
            }else{
                System.out.print(", "+v);
            }
            
        }
        System.out.println("");
    }
}
public class Day03RandNums {
    public static void main(String[] args){
        RandomStore rs= new RandomStore();
        int v1=rs.nextInt(1, 10);
        int v2=rs.nextInt(-100, -10);
        int v3=rs.nextInt(-20,21);
        System.out.printf("v1=%d, v2=%d, v3=%d\n", v1,v2,v3);
        rs.printHistory();
               
    }
    
}
