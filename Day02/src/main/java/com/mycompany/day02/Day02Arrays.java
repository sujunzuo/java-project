/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sujunzuo.day02names;

import java.util.Scanner;

/**
 *
 * @author fangzhai
 */
public class Day02Arrays {
    public static void main(String[] args){
        Scanner input=new Scanner(System.in);
        int i,level,j;
        int num_star,num_leftspace;
        
        System.out.print("How big does your tree needs to be? ");
        level=input.nextInt();
        
        
        for( i=1;i<=level;i++){
            //first count number of *
            num_star=2*i -1;
            num_leftspace=((2*level + 1)-num_star)/2;
            //begin print
            for(j=1;j<=num_leftspace;j++){
                System.out.print(" ");
            }
            for(j=1;j<=num_star;j++){
                System.out.print("*");
            }
            for(j=1;j<=num_leftspace;j++){
                System.out.print(" ");
            }
            System.out.println("");
        }
           
            
    }    
    
}
