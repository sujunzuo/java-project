/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sujunzuo.day02names;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 *
 * @author fangzhai
 */
public class Day02Numbers {
    /*public static void main(String[] args){
        Scanner input=new Scanner(System.in);
        int int_in,int_random;
        ArrayList<Integer> numList= new ArrayList<>();  
        Integer item;
      
        System.out.println("How many numbers do you want to generate:");
        int_in=input.nextInt();
        //Generate randam numbers
        for(int i=0;i<int_in;i++){
            int_random=(int)(Math.random()*201-100);
            numList.add(int_random);
        }
        
    
        
        //iterater
        Iterator myIterator=numList.iterator();
        while(myIterator.hasNext()){
            item=(Integer)myIterator.next();
            if(item.intValue()<=0)
              System.out.println(item.intValue());
            
        }

        
    } */
    // 1.rewrite the code so that instead of ArrayList it uses int[] array
    //2. when printing out positive numbers display them comma-separated
    //   in a single line.
    public static void main(String[] args){
        Scanner input=new Scanner(System.in);
        int int_in,int_random;
        boolean isFirst=true;
       
      
        System.out.print("How many numbers do you want to generate:");
        int_in=input.nextInt();
        int[] numList=new int[int_in];
        //Generate randam numbers
        for(int i=0;i<int_in;i++){
            int_random=(int)(Math.random()*201-100);
            numList[i]=int_random;
        }
        
    
        
        //iterater
        for(int v: numList){
            if(v >=0){
                System.out.print((isFirst?"":",") + v);
                isFirst=false;
            }
        }
    }
}
