/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day15sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

/**
 *
 * @author mobileapps
 */
public class Day15Sorting {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner input= new Scanner(System.in);
        String strInput="";
           /***********************************      
         //Part A
        String[] strList= new String[5];
        
        for(int i=0;i<5;i++){
            System.out.print("Pleas input a name please:");
            strInput=input.nextLine();
            strList[i]=strInput;
            
        }
        Arrays.sort(strList);

        for (String location : strList) {
            System.out.println("Name is: " + location);
        }
          */ 
           
        /***********************************
         Part B
           ArrayList<String> strList=new ArrayList<>();
        
        for(int i=0;i<5;i++){
            System.out.print("Pleas input a name please:");
            strInput=input.nextLine();
            strList.add(strInput);
            
        }
        Collections.sort(strList);

        for (String location : strList) {
            System.out.println("Name is: " + location);
        }
          */
        // new 6 cities
        ArrayList<City> cityList = new ArrayList<>();
        
        cityList.add(new City("Montreal",3.89,100));
        
        cityList.add(new City("Toronto",7.19,100));
        cityList.add(new City("Ottawa",0.99,100));
        cityList.add(new City("Quebec",0.79,200));
        cityList.add(new City("Sherbrooke",0.59,200));
        cityList.add(new City("Laval",0.79,200));
        
        //part C item 1 using Comparable interface
        System.out.println("Sorting by name using Comparable interface");
        Collections.sort(cityList);
        for(City city :cityList){
            System.out.println(city);
        }
        
        //part C item 2 using Comparator interface to sort city's population
        
        Collections.sort(cityList,new CompareCityByPopulation());
        System.out.println("Sorting by population using Comparator");
        
        for(City city :cityList){
            System.out.println(city);
        }
        //part C item 2 using Comparator interface to sort city's population
         System.out.println("Sorting by population using Comparator");
        Collections.sort(cityList,City.COMPARE_BY_POPULATION);
        for(City city :cityList){
            System.out.println(city);
        }
        //part C item 3using Comparator interface to sort city's elevation
        System.out.println("Sorting by Eelevation  using Comparator:");
        Collections.sort(cityList,City.COMPARE_BY_Elevation);
        for(City city :cityList){
            System.out.println(city);
        }
        // sort  by Java 8 Comparator.comparing
        System.out.println("Sorting by Eelevation, then by Name :");
        cityList.sort(Comparator.comparingInt(City::getElevationMeters).thenComparing(City::getName));
        for(City city :cityList){
            System.out.println(city);
        }
        // sort  by Java 8 Comparator.comparing
        System.out.println("sort  by Java 8 Comparator.comparing: Sorting by  Name :");
        cityList.sort(Comparator.comparing(City::getName));
        for(City city :cityList){
            System.out.println(city);
        }
    }
    
}
class City implements Comparable<City>{
    String name;
    double populationMillions;
    int elevationMeters;

    public City(String name, double populationMillions, int elevationMeters) {
        this.name = name;
        this.populationMillions = populationMillions;
        this.elevationMeters = elevationMeters;
    }

    @Override
    public String toString() {
        return "City{" + "name=" + name + ", populationMillions=" + populationMillions + ", elevationMeters=" + elevationMeters + '}';
    }

    @Override
    public int compareTo(City o) {
        return this.name.compareTo(o.name);
        
    }

    public String getName() {
        return name;
    }

    public double getPopulationMillions() {
        return populationMillions;
    }

    public int getElevationMeters() {
        return elevationMeters;
    }
    
     public final static Comparator<City> COMPARE_BY_POPULATION = new Comparator<City>() {
        public int compare(City first, City second){
            return first.populationMillions > second.populationMillions ? 1: first.populationMillions < second.populationMillions ? -1:0;
        }
    };
    public final static Comparator<City> COMPARE_BY_Elevation = new Comparator<City>() {
        public int compare(City first, City second){
            return first.elevationMeters > second.elevationMeters ? 1: first.elevationMeters < second.elevationMeters ? -1:0;
        }
    };
    
}
class CompareCityByPopulation implements Comparator<City>{

    @Override
    public int compare(City o1, City o2) {
        if(o1.populationMillions == o2.populationMillions)
            return 0;
        if(o1.populationMillions > o2.populationMillions)
            return 1;
        else
            return -1;
        
    }
    
}
